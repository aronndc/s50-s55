import { Row, Col, Button } from "react-bootstrap";

function PageNotFound() {
    return (
        <Row>
      <Col className="p-5">
        <h1>Page not Found!</h1>
        <p>Go back to <Button>Homepage</Button></p>
      </Col>
    </Row>
    );
}

export default PageNotFound;
import Banner from '../components/Banner';


export default function ErrorPage(){

    const bannerData = {
        title: 'Error 404',
        content: 'Page Not Found',
        destination: './',
        label: 'Back to HomePage'
    }

    return(
        <>
            <Banner bannerProp={bannerData}/>
        </>
    )
}
import {Fragment} from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){
    const bannerData = {
        title: 'Zuitt Coding Bootcamp',
        content: 'Opportunities for everyone, everywhere',
        destination: './courses',
        label: 'Enroll Now'
    }


    return(
        <Fragment>
            <Banner bannerProp={bannerData}/>
            <Highlights />
        </Fragment>
    )
}